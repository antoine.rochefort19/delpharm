#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "tBDD.h"
#include "Mes_Structures.h"
#include "tbalance.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void ClearTextAddUser();

    void ClearTextAddProd();

    void ClearTextAddRapport();

    void RemplirComboBox();

    void CacherLabel();

private slots:
    void on_Quittez_clicked();

    void on_Quitter_3_clicked();

    void on_pushButton_clicked();

    void on_Ajouter_clicked();


    void on_AjouterProduitRef_clicked();

    void on_comboBoxProduit_activated(const QString &arg1);

    void on_comboBoxProduit_currentIndexChanged(int index);

    void on_pushButton_2_clicked();

    void on_Envoyer_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::MainWindow *ui;
    tBDD *bdd;
    tbalance *Balance;
    //sProduit *Liste_Produit;
};

#endif // MAINWINDOW_H
