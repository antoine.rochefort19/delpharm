#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qcustomplot.h"
#include "tBDD.h"
#include <regex>
#include <QDebug>
#include <string>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QLabel *logo_delpharm = new QLabel;

    // CONNEXION A LA BDD

    bdd = new tBDD("172.16.185.202","Delpharm_user", "dNs903*NoNeck", "Delpharm"); //CONNEXION LYCEE
    //bdd = new tBDD("90.35.38.255", "Delpharm_user", "dNs903*NoNeck", "Delpharm"); CONNEXION MAISON

    // CONNEXION AVEC L'API DE LA BALANCE

    Balance = new tbalance("http://172.16.185.202:3000/api/balance");

    // AJOUT DES LOGOS SUR L'IHM

    ui->label->setPixmap(QPixmap("/home/etu/Images/logoDelpharm.png"));
    ui->label->show();

    ui->label_6->setPixmap(QPixmap("/home/etu/Images/logoDelpharm.png"));
    ui->label_6->show();

    ui->label_8->setPixmap(QPixmap("/home/etu/Images/logoDelpharm.png"));

    // ON REGLE LA TAILLE DES SPINS BOX

    ui->spinBoxPoste->setRange(1, 50);
    ui->spinBoxID->setRange(0001,1000);

    // ON APPEL CACHER LABEL ET REMPLIR COMBO BOX

    CacherLabel();

    RemplirComboBox();
}

// METHODE POUR REMPLIR LES COMBO BOX AVEC LE NOM DES PRODUITS DE LA BASE DE DONNÉE

void MainWindow::RemplirComboBox()
{

    sProduit* Liste_Produit = bdd->Selectionner_All_Produit();

    for (int i=0; i < bdd->Nb_Lignes_Liste ;i ++)
    {
        ui->comboBoxProdStat->addItem(Liste_Produit[i].Nom.c_str());
    }

    for (int i =0; i < bdd->Nb_Lignes_Liste; i++)
    {
        ui->comboBoxProduit->addItem(Liste_Produit[i].Nom.c_str());
    }
}

// METHODE POUR CACHER TOUT LES LABEL

void MainWindow::CacherLabel()
{

    ui->ErreurSaisie->hide();
    ui->UtilisateurAdd->hide();
    ui->ErreurAjouter->hide();
    ui->ErreurProduitRef->hide();
    ui->ProduitAdd->hide();
    ui->Conforme->hide();
    ui->NonConforme->hide();
    ui->RapportAdd->hide();
    ui->ErreurRap->hide();
}

MainWindow::~MainWindow()
{
    delete ui;
}

// METHODES POUR LES BOUTONS QUITTER SUR LES DIFFERENTS ONGLETS

void MainWindow::on_Quittez_clicked()
{
    exit(0);
}

void MainWindow::on_Quitter_3_clicked()
{
    exit(0);
}

void MainWindow::on_pushButton_clicked()
{
    exit(0);
}

// METHODE POUR CLEAR LE TEXT DANS L'ONGLET AJOUTER UN UTILASATEUR

void MainWindow::ClearTextAddUser()
{
    ui->lineEditNom->clear();
    ui->lineEditPrenom->clear();
    ui->lineEditMail->clear();
    ui->spinBoxID->clear();
}

// METHODE POUR CLEAR LE TEXT DANS L'ONGLET AJOUTER UN PRODUIT

void MainWindow::ClearTextAddProd()
{
    ui->lineEditPoidsRef->clear();
    ui->lineEditRefProduit->clear();
    ui->lineEditNomProduit->clear();
}

// METHODE POUR CLEAR LE TEXT DANS L'ONGLET AJOUTER UN RAPPORT

void MainWindow::ClearTextAddRapport()
{
    ui->NumControleur->clear();
    ui->lineEditService->clear();
    ui->PoidsProduit->clear();
    ui->PoidsRef->clear();
    ui->Variation->clear();
    ui->spinBoxPoste->clear();
    ui->NbProduit->clear();
}

// METHODE POUR AJOUTER UN UTILISATEUR DANS LA BDD QUAND LE BOUTON EST CLIQUÉ

void MainWindow::on_Ajouter_clicked()
{

    // CRÉATION DES VARIABLES QUI ONT COMME VALEUR LES CHAMPS ENTRÉ PAR LE CONTROLEUR

    string prenom = ui->lineEditPrenom->text().toStdString();
    string nom =ui->lineEditNom->text().toStdString();
    string mail = ui->lineEditMail->text().toStdString();

    int id = ui->spinBoxID->value();

    // REGEX POUR CONTROLER LA SAISIE DU CONTROLEUR

    regex expre_prenom("(.*)^[A-Z][a-z]{1,}$(.*)");
    bool match_prenom = regex_match(prenom.begin(), prenom.end(), expre_prenom);

    regex expre_nom("(.*)^[A-Z][a-z]{1,}$(.*)");
    bool match_nom = regex_match(nom.begin(), nom.end(), expre_nom);

    regex expre_mail("(.*)^[A-Za-z](\\w|\\d|\\.){1,}\\@[A-Za-z]{1,}\\.(com|fr)$(.*)");
    bool match_mail = regex_match(mail.begin(), mail.end(), expre_mail);

    // TEST SI LE MATCH DU PRENOM, DU NOM ET DU MAIL SONT FAUT ALORS UN MESSAGE D'ERREUR APPARAIT SINON SI AJOUTER USER EST VRAI, L'UTILISATEUR EST AJOUTÉ À LA BDD SINON UN MSG D'ERREUR APPARAIT

    if ((match_prenom == false) || (match_nom == false) || (match_mail == false))
    {
        ui->ErreurSaisie->show();
    }
    else
    {
        if (bdd->Ajouter_User(id,nom,prenom,mail) == 0)
        {
            ui->UtilisateurAdd->show();
            ui->ErreurSaisie->hide();
            ClearTextAddUser();
        }
    }

}

// METHODE POUR AJOUTER UN PRODUIT DE RÉFÉRENCE DANS LA BDD

void MainWindow::on_AjouterProduitRef_clicked()
{

    // CRÉATION DES VARIABLES QUI ONT COMME VALEUR LES CHAMPS ENTRÉ PAR LE CONTROLEUR

    string poidsRef = ui->lineEditPoidsRef->text().toStdString();
    string refProd = ui->lineEditRefProduit->text().toStdString();
    string NomProd = ui->lineEditNomProduit->text().toStdString();

    // REGEX POUR CONTROLER LA SAISIE DU CONTROLEUR

    regex expre_poidsRef("(.*)^\\d{1,}\\.\\d{1,}$(.*)");
    bool match_poidsRef = regex_match(poidsRef.begin(), poidsRef.end(), expre_poidsRef);

    regex expre_refProd("(.*)^[0-9]{2}[A-Z][0-9]{2}[A-Z]$(.*)");
    bool match_reference = regex_match(refProd.begin(), refProd.end(), expre_refProd);

    regex expre_NomProd("(.*)^[A-Z]\\w{1,}$(.*)");
    bool match_nomProd = regex_match(NomProd.begin(), NomProd.end(), expre_NomProd);

    // TEST SI LE MATCH DU POIDS, DE LA RÉFÉRENCE ET DU NOM SONT FAUT ALORS UN MESSAGE D'ERREUR APPARAIT SINON SI AJOUTER PRODUIT EST VRAI, LE PRODUIT EST AJOUTÉ À LA BDD

    if ((match_poidsRef == false) || (match_reference == false) || (match_nomProd == false))
    {
        ui->ErreurProduitRef->show();
    }
    else
    {
        if (bdd->Ajouter_Produit(refProd, poidsRef, NomProd) != false)
        {
            ui->ProduitAdd->show();
            ui->ErreurProduitRef->hide();
            ClearTextAddProd();
        }
    }
}

// METHODE POUR REMPLIR LE POIDS DE RÉFÉRENCE A PARTIR DE LA COMBO BOX DANS L'ONGLET FORMULAIRE LORSQUE L'ON CHANGE DE LIGNE DANS LA COMBO BOX

void MainWindow::on_comboBoxProduit_currentIndexChanged(int index)
{
    string Poids;

    sProduit* Liste_Produit = bdd->Selectionner_All_Produit();
    Poids = (Liste_Produit[index].Poids);

    QString PoidsString = QString::fromStdString(Poids);

    ui->PoidsRef->setText(PoidsString);
}

// METHODE POUR AFFICHER LE POIDS DU PRODUIT SUR LA BALANCE, LE CONTROLEUR DOIT D'ABORD APPUYÉ SUR LE BOUTON PUIS APPUYER SUR PRINT POUR AFFICHER LE POIDS

void MainWindow::on_pushButton_3_clicked()
{
    QString Poids = QString::fromStdString(Balance->get_poids());
    ui->PoidsProduit->setText(Poids);
    float Variation;
    Variation = ui->PoidsProduit->text().toFloat() - ui->PoidsRef->text().toFloat();
    QString VariationString = QString::number(Variation, 'f', 2);
    ui->Variation->setText(VariationString);
}

// METHODE LORSQUE L'ON CLIQUE SUR LE BOUTON ACTUALISER LE POIDS DE RÉFÉRENCE ET LA VARIATION SONT ACTUALISÉ

void MainWindow::on_pushButton_2_clicked()
{
    float Variation;
    Variation = ui->PoidsProduit->text().toFloat() - ui->PoidsRef->text().toFloat();
    QString VariationString = QString::number(Variation, 'f', 2);
    ui->Variation->setText(VariationString);
}

// METHODE POUR AJOUTER UN RAPPORT DANS LA BDD QUAND LE BOUTON EST CLIQUÉ

void MainWindow::on_Envoyer_clicked()
{

    // CRÉATION DES VARIABLES QUI ONT COMME VALEUR LES CHAMPS ENTRÉ PAR LE CONTROLEUR

    sProduit* Liste_Produit = bdd->Selectionner_All_Produit();
    bool NbProduitConforme;

    int NumControleur = ui->NumControleur->text().toInt();
    qDebug() <<"Num controleur : " << NumControleur;

    char Production = ui->lineEditService->text()[0].toLatin1();
    qDebug() <<"Production : " << Production;

    short Poste = ui->spinBoxPoste->text().toShort();
    qDebug() <<"Poste : "<< Poste;

    string Ref_Produit = Liste_Produit[ui->comboBoxProduit->currentIndex()].Reference;
    qDebug() <<"Produit de ref : " << Ref_Produit.c_str();

    string Poids = ui->PoidsProduit->text().toStdString();
    qDebug() <<"Poids : "<< Poids.c_str();

    string Variation = ui->Variation->text().toStdString();
    qDebug() <<"Variation : " << Variation.c_str();

    int Nombre_Produit = ui->NbProduit->text().toInt();
    qDebug() <<"Nombre de produit : " << Nombre_Produit;

    // VARIABLES POUR LE TEST DE CONFORMITÉ

    float PoidsFloat = ui->PoidsProduit->text().toFloat();
    float PoidsConforme = stoi(Poids)*(0.5/100);

    float PoidsRefFloat = ui->PoidsRef->text().toFloat();

    float PoidsConformePlus = PoidsRefFloat + PoidsConforme;
    float PoidsConformeMoins = PoidsRefFloat - PoidsConforme;
    bool Conforme;

    // TEST DE CONFORMITÉ DU PRODUIT

    if (PoidsFloat <= PoidsConformePlus && PoidsFloat >= PoidsConformeMoins)
    {
        Conforme = true;
        ui->Conforme->show();
        ui->NonConforme->hide();
    }
    else//
    {
        Conforme = false;
        ui->Conforme->hide();
        ui->NonConforme->show();
    }

    // REGEX POUR LE RAPPORT

    string Prodstring = string(1, Production);
    regex expre_Production("(.*)^[A-H]$(.*)");
    bool match_Production = regex_match(Prodstring.begin(), Prodstring.end(), expre_Production);

    // TEST POUR VERIFIER SI LE NOMBRE DE PRODUIT EST COMPRIS ENTRE 1 ET 10

    if (Nombre_Produit > 0 && Nombre_Produit <11)
    {
        NbProduitConforme = true;
    }
    else
    {
        NbProduitConforme = false;
    }

    // SI LES MATCHS SONT FALSE ON AFFICHE UNE ERREUR SINON SI AJOUTER RAPPORT EST VRAI ON AJOUTE LE RAPPORT ET ON AFFICHE UN MESSAGE DE CONFIRMATION SINON ON AFFICHE UN MESSAGE D'ERREUR

    if((match_Production == false) || (NbProduitConforme == false))
    {
        ui->ErreurRap->show();
    }
    else
    {
        if (bdd->Ajouter_Rapport(NumControleur, Production, Poste, Ref_Produit, Poids, Variation, Conforme, Nombre_Produit))
        {
            ui->RapportAdd->show();
            ui->ErreurRap->hide();
            //ClearTextAddRapport();
        }
        else
        {
            ui->ErreurRap->show();
        }
    }

}

