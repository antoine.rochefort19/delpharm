#ifndef TBDD_H
#define TBDD_H

#include <iostream>
#include <time.h>
#include <mysql/mysql.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

#include <string.h>
#include <ctime>

#include <array>
#include <list>

#include <Mes_Structures.h>

using namespace std;

class tBDD
{
private:

    char* Addr;
    char* Id;
    char* Mdp;
    char* Tab;

    string Addresse_IP;
    string Identifiant;
    string Mot_de_Passe;
    string Table;

    string Date_Heure;


    bool Request_MySql(string);

    string Get_Heure();

    void Connexion();


public:
    // LA PROGRAMMATION C'EST RIGOLO

    MYSQL* connexion;
    MYSQL_RES* Resultats;
    MYSQL_ROW Ligne;

    char Buffer_Requete_SQL[512];
    string Requete_SQL;
    // char Buffer_Time[512];
    short Nb_Lignes_Liste = 0;


    tBDD(string Addresse_IP, string Identifiant, string Mot_de_Passe, string table);

    //time_t Decouper_Temporalite();
    //time_t string_to_time_t(string);

    bool Ajouter_Rapport(int, char , short, string, string, string, bool, int);
    bool Ajouter_User(int, string, string, string);
    bool Ajouter_Produit(string, string, string);

    sControleur Selectionner_Controleur(int Identifiant);
    //array<sControleur,512> Selectionner_All_Controleur();
    sControleur* Selectionner_All_Controleur();


    sRapport Selectionner_Rapport(string Reference);
    //array<sRapport,512> Selectionner_All_Rapport();
    sRapport* Selectionner_All_Rapport();

    sProduit Selectionner_Produit(string Ref_Produit);
    //array<sProduit,512> Selectionner_All_Produit();
    sProduit* Selectionner_All_Produit();




    ~tBDD();
};

#endif // TBDD_H
