#ifndef MES_STRUCTURES_H
#define MES_STRUCTURES_H

#include <time.h>
#include <string>

using namespace std;

struct sControleur
{
    int Identifiant;
    string Nom;
    string Prenom;
    string Mail;
};

struct sRapport
{
    string Date_Heure;
    int Numero_Controleur;
    char Production;
    short Poste;
    string Reference_Produit;
    float Poids;
    float Variation;
    bool Conforme;
    int Nombre_Produit;

};

struct sProduit
{
    string Reference;
    string Poids;
    string Nom;
};

#endif // MES_STRUCTURES_H
