#include "tBDD.h"


using namespace std;

tBDD::tBDD(string Addresse_IP, string Identifiant, string Mot_de_Passe, string Table)
{

    Addr = new char[512];                       // Créer un char pour le champs de l'adresse IP lors de la connexion
    strncpy(Addr, Addresse_IP.c_str(), 512);

    Id = new char[512];                       // Créer un char pour le champs de l'identifiant lors de la connexion
    strncpy(Id, Identifiant.c_str(), 512);

    Mdp = new char[512];                       // Créer un char pour le champs de le mot de passe lors de la connexion
    strncpy(Mdp, Mot_de_Passe.c_str(), 512);

    Tab = new char[512];                       // Créer un char pour le champs de la table de la base de donnée lors de la connexion
    strncpy(Tab, Table.c_str(), 512);

    Connexion();        // Appelle la fonction Connexion()
}

void tBDD::Connexion()
{

    connexion = mysql_init(NULL);   // Initialise un objet MYSQL convenable pour mysql_real_connect()
    assert(connexion != NULL);      // Test que la connexion est nulle


     if(mysql_real_connect(connexion, Addr, Id, Mdp, Tab, 0, NULL, 0) == NULL)          // Connexion à la base de donnée
     {
         printf("Erreur Connexion Base de Données\n");
         exit(1);
     }
     printf("Connecté à la Base de Données\n");
}

bool tBDD::Request_MySql(string Sql_Request)
{
    char* Request = new char[Sql_Request.length()+1];
    strcpy(Request, Sql_Request.c_str());

    if(mysql_query(connexion, Request) != 0)
    {
        printf("Erreur lors de la requete\n");
        return false;
    }
    else
    {
        Resultats = mysql_store_result(connexion);
        return true;
    }


}

string tBDD::Get_Heure()
{
    time_t timer;
    struct tm info;

    char buff[26];

    time(&timer);
    info = *localtime(&timer);
    sprintf(buff,"%d-%d-%d %d:%d:%d",info.tm_year+1900,info.tm_mon+1,info.tm_mday,info.tm_hour,info.tm_min,info.tm_sec);

    return string(buff);
}

bool tBDD::Ajouter_User(int Identifiant, string Nom, string Prenom, string Mail)
{

    sprintf(Buffer_Requete_SQL, "INSERT INTO Controleur(Identifiant,Nom,Prenom,Mail) VALUES (%d,'%s','%s','%s')", Identifiant, Nom.c_str(), Prenom.c_str(), Mail.c_str());     // Ecrit la requete MySQL dans le char Request

    Requete_SQL = string(Buffer_Requete_SQL);
    return Request_MySql(Requete_SQL);

}

bool tBDD::Ajouter_Produit(string Reference, string Poids, string Nom)
{
    sprintf(Buffer_Requete_SQL, "INSERT INTO Produit_reference(Reference, Grammes, Nom) VALUES ('%s','%s','%s')", Reference.c_str(), Poids.c_str(), Nom.c_str());
    Requete_SQL = string(Buffer_Requete_SQL);
    return  Request_MySql(Requete_SQL);
}

bool tBDD::Ajouter_Rapport(int Controleur, char Production, short Poste, string Ref_Produit, string Poids, string Variation, bool Conforme, int Nombre_Produit)
{
    // Pas oublier de récupérer les autres variables
    string Set_Heure = Get_Heure();
    string NombreProduit = to_string(Nombre_Produit);
    cout << NombreProduit << endl;
    cout << Nombre_Produit << endl;
    sprintf(Buffer_Requete_SQL, "INSERT INTO Rapport(Controleur,Time,Production,Poste,Reference,Grammes_peser,Variation,Conforme,Nombre_produits) VALUES (%d,'%s','%c',%d,'%s','%s','%s',%d,%d)",
            Controleur, Set_Heure.c_str(), Production, Poste, Ref_Produit.c_str(), Poids.c_str(), Variation.c_str(), Conforme, Nombre_Produit);
    Requete_SQL = string(Buffer_Requete_SQL);
    return Request_MySql(Requete_SQL);

}

sControleur tBDD::Selectionner_Controleur(int Identifiant)
{
    sControleur Controleur;
    sprintf(Buffer_Requete_SQL, "SELECT * FROM Controleur WHERE identifiant = %d", Identifiant);       // Ecrit la requete MySQL dans le char Request
    Requete_SQL = string(Buffer_Requete_SQL);
    Request_MySql(Requete_SQL);

    if (Resultats == NULL)
    {
        printf("Pas de données dans résultats.\n");
    }
    else
    {
        Ligne = mysql_fetch_row(Resultats);

        Controleur.Identifiant = atoi(Ligne[0]);
        Controleur.Prenom = string(Ligne[1]);
        Controleur.Nom = string(Ligne[2]);
        Controleur.Mail = string(Ligne[3]);

    }

    return Controleur;
}

sRapport tBDD::Selectionner_Rapport(string Reference)
{
    sRapport Rapport;
    sprintf(Buffer_Requete_SQL, "SELECT * FROM Rapport WHERE Reference = '%s'", Reference.c_str());     // Ecrit la requete MySQL dans le char Request
    Requete_SQL = string(Buffer_Requete_SQL);
    Request_MySql(Requete_SQL);

    if (Resultats == NULL)
    {
        printf("Pas de données dans résultats.\n");
    }
    else
    {
        Ligne = mysql_fetch_row(Resultats);

        Rapport.Numero_Controleur = atoi(Ligne[1]);
        Rapport.Date_Heure = string(Ligne[2]);
        Rapport.Production = *Ligne[3];
        Rapport.Poste = atoi(Ligne[4]);
        Rapport.Reference_Produit = string(Ligne[5]);
        Rapport.Poids = atof(Ligne[6]);
        Rapport.Variation = atof(Ligne[7]);
        Rapport.Conforme = Ligne[8];
        Rapport.Nombre_Produit = atoi(Ligne[9]);

       // printf("Controleur: %s, Time: %s, Production: %s, Poste: %s, Reference: %s, Poids: %s, Variation: %s, Conforme: %s, Nombre_produits: %s\n", Ligne[1], Ligne[2], Ligne[3], Ligne[4], Ligne[5], Ligne[6], Ligne[7], Ligne[8], Ligne[9], Ligne[10]);    // Affiche le produit
    }


    return Rapport;

}

sProduit tBDD::Selectionner_Produit(string Ref_Produit)
{
    sProduit Produit;
    sprintf(Buffer_Requete_SQL, "SELECT * FROM Produit_reference WHERE Reference = '%s';", Ref_Produit.c_str());       // Ecrit la requete MySQL dans le char Request
    Requete_SQL = string(Buffer_Requete_SQL);
    Request_MySql(Requete_SQL);

    if (Resultats == NULL)
    {
        printf("Pas de données dans résultats.\n");
    }
    else
    {
        Ligne = mysql_fetch_row(Resultats);

        Produit.Reference = string(Ligne[0]);
        Produit.Poids = atof(Ligne[1]);
        Produit.Nom = string(Ligne[2]);
        printf("Reference: %s, Poids: %s, Nom: %s\n", Ligne[0], Ligne[1], Ligne[2]);    // Affiche le produit

    }

    return Produit;

}

sControleur* tBDD::Selectionner_All_Controleur()
{
    sControleur* Controleur;

    Request_MySql("SELECT * FROM Controleur");

    if (Resultats == NULL)
    {
        printf("Pas de données dans résultats");
    }
    else
    {
        Nb_Lignes_Liste = mysql_num_rows(Resultats);
        Controleur = new sControleur[Nb_Lignes_Liste];


        for (int i = 0; i < Nb_Lignes_Liste; i++)            // Va stocker tout les controleurs dans Liste_Controleur
        {
            Ligne = mysql_fetch_row(Resultats);

            Controleur[i].Identifiant = atoi(Ligne[0]);
            Controleur[i].Prenom = string(Ligne[1]);
            Controleur[i].Nom = string(Ligne[2]);
            Controleur[i].Mail = string(Ligne[3]);

         }

    }

    return Controleur;

}

sProduit* tBDD::Selectionner_All_Produit()
{
    sProduit* Produit;

    Request_MySql("SELECT * FROM Produit_reference");

    if (Resultats == NULL)
    {
        printf("Pas de données dans résultats");
    }
    else
    {
        Nb_Lignes_Liste = mysql_num_rows(Resultats);
        Produit = new sProduit[Nb_Lignes_Liste];


        for (int i = 0; i < Nb_Lignes_Liste; i++)            // Va stocker tout les controleurs dans Liste_Proquit
        {
            Ligne = mysql_fetch_row(Resultats);

            Produit[i].Reference = string(Ligne[0]);
            Produit[i].Poids = string(Ligne[1]);
            Produit[i].Nom = string(Ligne[2]);

         }


    }

    return Produit;
}

sRapport* tBDD::Selectionner_All_Rapport()
{
    sRapport* Rapport;

    Request_MySql("SELECT * FROM Rapport");


    if (Resultats == NULL)
    {
        printf("Pas de données dans résultats.\n");
    }
    else
    {
        Nb_Lignes_Liste = mysql_num_rows(Resultats);
        Rapport = new sRapport[Nb_Lignes_Liste];

        for (int i = 0; i < Nb_Lignes_Liste; i++)            // Va stocker tout les controleurs dans Liste_Proquit
        {
            Ligne = mysql_fetch_row(Resultats);
            //printf("Controleur: %s, Time: %s, Production: %s, Poste: %s, Reference: %s, Poids: %s, Variation: %s, Conforme: %s, Nombre_produits: %s\n", Ligne[1], Ligne[2], Ligne[3], Ligne[4], Ligne[5], Ligne[6], Ligne[7], Ligne[8], Ligne[9], Ligne[10]);    // Affiche le produit

            Rapport[i].Numero_Controleur = atoi(Ligne[1]);
            Rapport[i].Date_Heure = string(Ligne[2]);
            Rapport[i].Production = *Ligne[3];
            Rapport[i].Poste = atoi(Ligne[4]);
            Rapport[i].Reference_Produit = string(Ligne[5]);
            Rapport[i].Poids = atof(Ligne[6]);
            Rapport[i].Variation = atof(Ligne[7]);
            *Ligne[8] == '0' ? Rapport[i].Conforme = false:Rapport[i].Conforme = true;
            Rapport[i].Nombre_Produit = atoi(Ligne[9]);


        }

        printf("Ma Liste: \n");
        for (int i = 0; i < Nb_Lignes_Liste; i++)        // Affiche la Liste_Produit
        {
            printf("Controleur: %d, Date: %s, Production: %c, Poste: %d, Reference: %s, Poids: %f, Variation: %f, Conforme: %c, Nombre_produits: %d\n", Rapport[i].Numero_Controleur, Rapport[i].Date_Heure.c_str(), Rapport[i].Production, Rapport[i].Poste, Rapport[i].Reference_Produit.c_str(), Rapport[i].Poids, Rapport[i].Variation, Rapport[i].Conforme, Rapport[i].Nombre_Produit);    // Affiche le produit
        }

    }

    return Rapport;
}

tBDD::~tBDD()
{

}

