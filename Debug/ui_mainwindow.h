/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QTabWidget *tabWidget;
    QWidget *Formulaire;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QLabel *Numero_du_controleur;
    QSpinBox *NumControleur;
    QWidget *horizontalLayoutWidget_4;
    QHBoxLayout *horizontalLayout_4;
    QLabel *Ref_du_produit;
    QComboBox *comboBoxProduit;
    QPushButton *Envoyer;
    QLabel *label;
    QPushButton *Quittez;
    QWidget *horizontalLayoutWidget_5;
    QHBoxLayout *horizontalLayout_5;
    QLabel *PoidsDuProduit;
    QLineEdit *PoidsProduit;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout_2;
    QLabel *Service;
    QLineEdit *lineEditService;
    QWidget *horizontalLayoutWidget_6;
    QHBoxLayout *horizontalLayout_6;
    QLabel *PoidsDeReference;
    QLineEdit *PoidsRef;
    QWidget *horizontalLayoutWidget_3;
    QHBoxLayout *horizontalLayout_3;
    QLabel *Nom_du_poste;
    QSpinBox *spinBoxPoste;
    QPushButton *IMPRIMER;
    QWidget *horizontalLayoutWidget_15;
    QHBoxLayout *horizontalLayout_15;
    QLabel *label_9;
    QLineEdit *Variation;
    QWidget *horizontalLayoutWidget_7;
    QHBoxLayout *horizontalLayout_7;
    QLabel *Conforme;
    QLabel *NonConforme;
    QWidget *horizontalLayoutWidget_17;
    QHBoxLayout *horizontalLayout_17;
    QLabel *NbDeProduit;
    QLineEdit *NbProduit;
    QWidget *horizontalLayoutWidget_18;
    QHBoxLayout *horizontalLayout_18;
    QLabel *RapportAdd;
    QLabel *ErreurRap;
    QWidget *horizontalLayoutWidget_19;
    QHBoxLayout *horizontalLayout_19;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QWidget *Produit_de_reference;
    QWidget *horizontalLayoutWidget_8;
    QHBoxLayout *horizontalLayout_8;
    QLabel *Nom_du_produit;
    QLineEdit *lineEditNomProduit;
    QWidget *horizontalLayoutWidget_9;
    QHBoxLayout *horizontalLayout_9;
    QLabel *Reference;
    QLineEdit *lineEditRefProduit;
    QWidget *horizontalLayoutWidget_14;
    QHBoxLayout *horizontalLayout_14;
    QLabel *PoidsProduitRef;
    QLineEdit *lineEditPoidsRef;
    QPushButton *AjouterProduitRef;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_2;
    QLabel *ErreurProduitRef;
    QLabel *ProduitAdd;
    QPushButton *pushButton;
    QLabel *label_8;
    QWidget *Utilisateur;
    QWidget *horizontalLayoutWidget_10;
    QHBoxLayout *horizontalLayout_10;
    QLabel *NomUser;
    QLineEdit *lineEditNom;
    QWidget *horizontalLayoutWidget_11;
    QHBoxLayout *horizontalLayout_11;
    QLabel *PrenomUser;
    QLineEdit *lineEditPrenom;
    QWidget *horizontalLayoutWidget_12;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_5;
    QLineEdit *lineEditMail;
    QLabel *label_6;
    QPushButton *Ajouter;
    QPushButton *Quitter_3;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *UtilisateurAdd;
    QLabel *ErreurAjouter;
    QLabel *ErreurSaisie;
    QWidget *horizontalLayoutWidget_13;
    QHBoxLayout *horizontalLayout_13;
    QLabel *label_7;
    QSpinBox *spinBoxID;
    QWidget *Statistiques;
    QCustomPlot *widget;
    QWidget *horizontalLayoutWidget_16;
    QHBoxLayout *horizontalLayout_16;
    QLabel *ProduitStat;
    QComboBox *comboBoxProdStat;
    QMenuBar *menuBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(767, 533);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setGeometry(QRect(10, 0, 741, 481));
        QFont font;
        font.setFamily(QStringLiteral("Trebuchet MS"));
        tabWidget->setFont(font);
        Formulaire = new QWidget();
        Formulaire->setObjectName(QStringLiteral("Formulaire"));
        Formulaire->setEnabled(true);
        horizontalLayoutWidget = new QWidget(Formulaire);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(10, 10, 241, 61));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        Numero_du_controleur = new QLabel(horizontalLayoutWidget);
        Numero_du_controleur->setObjectName(QStringLiteral("Numero_du_controleur"));
        QFont font1;
        font1.setFamily(QStringLiteral("Trebuchet MS"));
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setWeight(75);
        Numero_du_controleur->setFont(font1);

        horizontalLayout->addWidget(Numero_du_controleur);

        NumControleur = new QSpinBox(horizontalLayoutWidget);
        NumControleur->setObjectName(QStringLiteral("NumControleur"));
        NumControleur->setMaximum(1000);

        horizontalLayout->addWidget(NumControleur);

        horizontalLayoutWidget_4 = new QWidget(Formulaire);
        horizontalLayoutWidget_4->setObjectName(QStringLiteral("horizontalLayoutWidget_4"));
        horizontalLayoutWidget_4->setGeometry(QRect(10, 200, 271, 61));
        horizontalLayout_4 = new QHBoxLayout(horizontalLayoutWidget_4);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(0, 0, 0, 0);
        Ref_du_produit = new QLabel(horizontalLayoutWidget_4);
        Ref_du_produit->setObjectName(QStringLiteral("Ref_du_produit"));
        Ref_du_produit->setFont(font1);

        horizontalLayout_4->addWidget(Ref_du_produit);

        comboBoxProduit = new QComboBox(horizontalLayoutWidget_4);
        comboBoxProduit->setObjectName(QStringLiteral("comboBoxProduit"));

        horizontalLayout_4->addWidget(comboBoxProduit);

        Envoyer = new QPushButton(Formulaire);
        Envoyer->setObjectName(QStringLiteral("Envoyer"));
        Envoyer->setGeometry(QRect(30, 330, 161, 41));
        Envoyer->setFont(font1);
        label = new QLabel(Formulaire);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(630, 0, 111, 111));
        label->setPixmap(QPixmap(QString::fromUtf8("../../../Images/logoDelpharm.png")));
        Quittez = new QPushButton(Formulaire);
        Quittez->setObjectName(QStringLiteral("Quittez"));
        Quittez->setGeometry(QRect(620, 400, 86, 28));
        Quittez->setFont(font1);
        horizontalLayoutWidget_5 = new QWidget(Formulaire);
        horizontalLayoutWidget_5->setObjectName(QStringLiteral("horizontalLayoutWidget_5"));
        horizontalLayoutWidget_5->setGeometry(QRect(330, 10, 261, 61));
        horizontalLayout_5 = new QHBoxLayout(horizontalLayoutWidget_5);
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(0, 0, 0, 0);
        PoidsDuProduit = new QLabel(horizontalLayoutWidget_5);
        PoidsDuProduit->setObjectName(QStringLiteral("PoidsDuProduit"));
        PoidsDuProduit->setFont(font1);

        horizontalLayout_5->addWidget(PoidsDuProduit);

        PoidsProduit = new QLineEdit(horizontalLayoutWidget_5);
        PoidsProduit->setObjectName(QStringLiteral("PoidsProduit"));
        PoidsProduit->setReadOnly(true);

        horizontalLayout_5->addWidget(PoidsProduit);

        horizontalLayoutWidget_2 = new QWidget(Formulaire);
        horizontalLayoutWidget_2->setObjectName(QStringLiteral("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(10, 80, 241, 51));
        horizontalLayout_2 = new QHBoxLayout(horizontalLayoutWidget_2);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        Service = new QLabel(horizontalLayoutWidget_2);
        Service->setObjectName(QStringLiteral("Service"));
        Service->setFont(font1);

        horizontalLayout_2->addWidget(Service);

        lineEditService = new QLineEdit(horizontalLayoutWidget_2);
        lineEditService->setObjectName(QStringLiteral("lineEditService"));

        horizontalLayout_2->addWidget(lineEditService);

        horizontalLayoutWidget_6 = new QWidget(Formulaire);
        horizontalLayoutWidget_6->setObjectName(QStringLiteral("horizontalLayoutWidget_6"));
        horizontalLayoutWidget_6->setGeometry(QRect(330, 80, 261, 61));
        horizontalLayout_6 = new QHBoxLayout(horizontalLayoutWidget_6);
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(0, 0, 0, 0);
        PoidsDeReference = new QLabel(horizontalLayoutWidget_6);
        PoidsDeReference->setObjectName(QStringLiteral("PoidsDeReference"));
        PoidsDeReference->setFont(font1);

        horizontalLayout_6->addWidget(PoidsDeReference);

        PoidsRef = new QLineEdit(horizontalLayoutWidget_6);
        PoidsRef->setObjectName(QStringLiteral("PoidsRef"));
        PoidsRef->setReadOnly(true);

        horizontalLayout_6->addWidget(PoidsRef);

        horizontalLayoutWidget_3 = new QWidget(Formulaire);
        horizontalLayoutWidget_3->setObjectName(QStringLiteral("horizontalLayoutWidget_3"));
        horizontalLayoutWidget_3->setGeometry(QRect(10, 140, 241, 51));
        horizontalLayout_3 = new QHBoxLayout(horizontalLayoutWidget_3);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        Nom_du_poste = new QLabel(horizontalLayoutWidget_3);
        Nom_du_poste->setObjectName(QStringLiteral("Nom_du_poste"));
        Nom_du_poste->setFont(font1);

        horizontalLayout_3->addWidget(Nom_du_poste);

        spinBoxPoste = new QSpinBox(horizontalLayoutWidget_3);
        spinBoxPoste->setObjectName(QStringLiteral("spinBoxPoste"));

        horizontalLayout_3->addWidget(spinBoxPoste);

        IMPRIMER = new QPushButton(Formulaire);
        IMPRIMER->setObjectName(QStringLiteral("IMPRIMER"));
        IMPRIMER->setGeometry(QRect(30, 380, 161, 41));
        IMPRIMER->setFont(font1);
        horizontalLayoutWidget_15 = new QWidget(Formulaire);
        horizontalLayoutWidget_15->setObjectName(QStringLiteral("horizontalLayoutWidget_15"));
        horizontalLayoutWidget_15->setGeometry(QRect(330, 150, 261, 61));
        horizontalLayout_15 = new QHBoxLayout(horizontalLayoutWidget_15);
        horizontalLayout_15->setSpacing(6);
        horizontalLayout_15->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_15->setObjectName(QStringLiteral("horizontalLayout_15"));
        horizontalLayout_15->setContentsMargins(0, 0, 0, 0);
        label_9 = new QLabel(horizontalLayoutWidget_15);
        label_9->setObjectName(QStringLiteral("label_9"));
        QFont font2;
        font2.setPointSize(12);
        font2.setBold(true);
        font2.setWeight(75);
        label_9->setFont(font2);

        horizontalLayout_15->addWidget(label_9);

        Variation = new QLineEdit(horizontalLayoutWidget_15);
        Variation->setObjectName(QStringLiteral("Variation"));
        Variation->setEnabled(true);
        Variation->setReadOnly(true);

        horizontalLayout_15->addWidget(Variation);

        horizontalLayoutWidget_7 = new QWidget(Formulaire);
        horizontalLayoutWidget_7->setObjectName(QStringLiteral("horizontalLayoutWidget_7"));
        horizontalLayoutWidget_7->setGeometry(QRect(330, 270, 251, 41));
        horizontalLayout_7 = new QHBoxLayout(horizontalLayoutWidget_7);
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        horizontalLayout_7->setContentsMargins(0, 0, 0, 0);
        Conforme = new QLabel(horizontalLayoutWidget_7);
        Conforme->setObjectName(QStringLiteral("Conforme"));
        Conforme->setFont(font1);

        horizontalLayout_7->addWidget(Conforme);

        NonConforme = new QLabel(horizontalLayoutWidget_7);
        NonConforme->setObjectName(QStringLiteral("NonConforme"));
        NonConforme->setFont(font1);

        horizontalLayout_7->addWidget(NonConforme);

        horizontalLayoutWidget_17 = new QWidget(Formulaire);
        horizontalLayoutWidget_17->setObjectName(QStringLiteral("horizontalLayoutWidget_17"));
        horizontalLayoutWidget_17->setGeometry(QRect(10, 270, 241, 41));
        horizontalLayout_17 = new QHBoxLayout(horizontalLayoutWidget_17);
        horizontalLayout_17->setSpacing(6);
        horizontalLayout_17->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_17->setObjectName(QStringLiteral("horizontalLayout_17"));
        horizontalLayout_17->setContentsMargins(0, 0, 0, 0);
        NbDeProduit = new QLabel(horizontalLayoutWidget_17);
        NbDeProduit->setObjectName(QStringLiteral("NbDeProduit"));
        NbDeProduit->setFont(font2);

        horizontalLayout_17->addWidget(NbDeProduit);

        NbProduit = new QLineEdit(horizontalLayoutWidget_17);
        NbProduit->setObjectName(QStringLiteral("NbProduit"));

        horizontalLayout_17->addWidget(NbProduit);

        horizontalLayoutWidget_18 = new QWidget(Formulaire);
        horizontalLayoutWidget_18->setObjectName(QStringLiteral("horizontalLayoutWidget_18"));
        horizontalLayoutWidget_18->setGeometry(QRect(330, 320, 274, 31));
        horizontalLayout_18 = new QHBoxLayout(horizontalLayoutWidget_18);
        horizontalLayout_18->setSpacing(6);
        horizontalLayout_18->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_18->setObjectName(QStringLiteral("horizontalLayout_18"));
        horizontalLayout_18->setContentsMargins(0, 0, 0, 0);
        RapportAdd = new QLabel(horizontalLayoutWidget_18);
        RapportAdd->setObjectName(QStringLiteral("RapportAdd"));
        RapportAdd->setFont(font2);

        horizontalLayout_18->addWidget(RapportAdd);

        ErreurRap = new QLabel(horizontalLayoutWidget_18);
        ErreurRap->setObjectName(QStringLiteral("ErreurRap"));
        ErreurRap->setFont(font2);

        horizontalLayout_18->addWidget(ErreurRap);

        horizontalLayoutWidget_19 = new QWidget(Formulaire);
        horizontalLayoutWidget_19->setObjectName(QStringLiteral("horizontalLayoutWidget_19"));
        horizontalLayoutWidget_19->setGeometry(QRect(330, 220, 326, 41));
        horizontalLayout_19 = new QHBoxLayout(horizontalLayoutWidget_19);
        horizontalLayout_19->setSpacing(6);
        horizontalLayout_19->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_19->setObjectName(QStringLiteral("horizontalLayout_19"));
        horizontalLayout_19->setContentsMargins(0, 0, 0, 0);
        pushButton_2 = new QPushButton(horizontalLayoutWidget_19);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setFont(font2);

        horizontalLayout_19->addWidget(pushButton_2);

        pushButton_3 = new QPushButton(horizontalLayoutWidget_19);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setFont(font2);

        horizontalLayout_19->addWidget(pushButton_3);

        tabWidget->addTab(Formulaire, QString());
        Produit_de_reference = new QWidget();
        Produit_de_reference->setObjectName(QStringLiteral("Produit_de_reference"));
        horizontalLayoutWidget_8 = new QWidget(Produit_de_reference);
        horizontalLayoutWidget_8->setObjectName(QStringLiteral("horizontalLayoutWidget_8"));
        horizontalLayoutWidget_8->setGeometry(QRect(10, 150, 371, 61));
        horizontalLayout_8 = new QHBoxLayout(horizontalLayoutWidget_8);
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        horizontalLayout_8->setContentsMargins(0, 0, 0, 0);
        Nom_du_produit = new QLabel(horizontalLayoutWidget_8);
        Nom_du_produit->setObjectName(QStringLiteral("Nom_du_produit"));
        Nom_du_produit->setFont(font1);

        horizontalLayout_8->addWidget(Nom_du_produit);

        lineEditNomProduit = new QLineEdit(horizontalLayoutWidget_8);
        lineEditNomProduit->setObjectName(QStringLiteral("lineEditNomProduit"));

        horizontalLayout_8->addWidget(lineEditNomProduit);

        horizontalLayoutWidget_9 = new QWidget(Produit_de_reference);
        horizontalLayoutWidget_9->setObjectName(QStringLiteral("horizontalLayoutWidget_9"));
        horizontalLayoutWidget_9->setGeometry(QRect(10, 80, 371, 61));
        horizontalLayout_9 = new QHBoxLayout(horizontalLayoutWidget_9);
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        horizontalLayout_9->setContentsMargins(0, 0, 0, 0);
        Reference = new QLabel(horizontalLayoutWidget_9);
        Reference->setObjectName(QStringLiteral("Reference"));
        Reference->setFont(font2);

        horizontalLayout_9->addWidget(Reference);

        lineEditRefProduit = new QLineEdit(horizontalLayoutWidget_9);
        lineEditRefProduit->setObjectName(QStringLiteral("lineEditRefProduit"));

        horizontalLayout_9->addWidget(lineEditRefProduit);

        horizontalLayoutWidget_14 = new QWidget(Produit_de_reference);
        horizontalLayoutWidget_14->setObjectName(QStringLiteral("horizontalLayoutWidget_14"));
        horizontalLayoutWidget_14->setGeometry(QRect(10, 10, 371, 61));
        horizontalLayout_14 = new QHBoxLayout(horizontalLayoutWidget_14);
        horizontalLayout_14->setSpacing(6);
        horizontalLayout_14->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_14->setObjectName(QStringLiteral("horizontalLayout_14"));
        horizontalLayout_14->setContentsMargins(0, 0, 0, 0);
        PoidsProduitRef = new QLabel(horizontalLayoutWidget_14);
        PoidsProduitRef->setObjectName(QStringLiteral("PoidsProduitRef"));
        PoidsProduitRef->setFont(font2);

        horizontalLayout_14->addWidget(PoidsProduitRef);

        lineEditPoidsRef = new QLineEdit(horizontalLayoutWidget_14);
        lineEditPoidsRef->setObjectName(QStringLiteral("lineEditPoidsRef"));

        horizontalLayout_14->addWidget(lineEditPoidsRef);

        AjouterProduitRef = new QPushButton(Produit_de_reference);
        AjouterProduitRef->setObjectName(QStringLiteral("AjouterProduitRef"));
        AjouterProduitRef->setGeometry(QRect(90, 250, 151, 81));
        AjouterProduitRef->setFont(font1);
        verticalLayoutWidget_2 = new QWidget(Produit_de_reference);
        verticalLayoutWidget_2->setObjectName(QStringLiteral("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(480, 140, 160, 80));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        ErreurProduitRef = new QLabel(verticalLayoutWidget_2);
        ErreurProduitRef->setObjectName(QStringLiteral("ErreurProduitRef"));
        ErreurProduitRef->setFont(font2);

        verticalLayout_2->addWidget(ErreurProduitRef);

        ProduitAdd = new QLabel(verticalLayoutWidget_2);
        ProduitAdd->setObjectName(QStringLiteral("ProduitAdd"));
        ProduitAdd->setFont(font2);

        verticalLayout_2->addWidget(ProduitAdd);

        pushButton = new QPushButton(Produit_de_reference);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(620, 400, 86, 28));
        pushButton->setFont(font2);
        label_8 = new QLabel(Produit_de_reference);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(620, 0, 111, 111));
        label_8->setPixmap(QPixmap(QString::fromUtf8("../../../Images/logoDelpharm.png")));
        tabWidget->addTab(Produit_de_reference, QString());
        Utilisateur = new QWidget();
        Utilisateur->setObjectName(QStringLiteral("Utilisateur"));
        horizontalLayoutWidget_10 = new QWidget(Utilisateur);
        horizontalLayoutWidget_10->setObjectName(QStringLiteral("horizontalLayoutWidget_10"));
        horizontalLayoutWidget_10->setGeometry(QRect(10, 20, 331, 61));
        horizontalLayout_10 = new QHBoxLayout(horizontalLayoutWidget_10);
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        horizontalLayout_10->setContentsMargins(0, 0, 0, 0);
        NomUser = new QLabel(horizontalLayoutWidget_10);
        NomUser->setObjectName(QStringLiteral("NomUser"));
        NomUser->setFont(font1);

        horizontalLayout_10->addWidget(NomUser);

        lineEditNom = new QLineEdit(horizontalLayoutWidget_10);
        lineEditNom->setObjectName(QStringLiteral("lineEditNom"));

        horizontalLayout_10->addWidget(lineEditNom);

        horizontalLayoutWidget_11 = new QWidget(Utilisateur);
        horizontalLayoutWidget_11->setObjectName(QStringLiteral("horizontalLayoutWidget_11"));
        horizontalLayoutWidget_11->setGeometry(QRect(10, 100, 331, 61));
        horizontalLayout_11 = new QHBoxLayout(horizontalLayoutWidget_11);
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        horizontalLayout_11->setContentsMargins(0, 0, 0, 0);
        PrenomUser = new QLabel(horizontalLayoutWidget_11);
        PrenomUser->setObjectName(QStringLiteral("PrenomUser"));
        PrenomUser->setFont(font1);

        horizontalLayout_11->addWidget(PrenomUser);

        lineEditPrenom = new QLineEdit(horizontalLayoutWidget_11);
        lineEditPrenom->setObjectName(QStringLiteral("lineEditPrenom"));

        horizontalLayout_11->addWidget(lineEditPrenom);

        horizontalLayoutWidget_12 = new QWidget(Utilisateur);
        horizontalLayoutWidget_12->setObjectName(QStringLiteral("horizontalLayoutWidget_12"));
        horizontalLayoutWidget_12->setGeometry(QRect(10, 180, 331, 61));
        horizontalLayout_12 = new QHBoxLayout(horizontalLayoutWidget_12);
        horizontalLayout_12->setSpacing(6);
        horizontalLayout_12->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_12->setObjectName(QStringLiteral("horizontalLayout_12"));
        horizontalLayout_12->setContentsMargins(0, 0, 0, 0);
        label_5 = new QLabel(horizontalLayoutWidget_12);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setFont(font1);

        horizontalLayout_12->addWidget(label_5);

        lineEditMail = new QLineEdit(horizontalLayoutWidget_12);
        lineEditMail->setObjectName(QStringLiteral("lineEditMail"));

        horizontalLayout_12->addWidget(lineEditMail);

        label_6 = new QLabel(Utilisateur);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(620, 0, 111, 111));
        label_6->setPixmap(QPixmap(QString::fromUtf8("../../../Images/logoDelpharm.png")));
        Ajouter = new QPushButton(Utilisateur);
        Ajouter->setObjectName(QStringLiteral("Ajouter"));
        Ajouter->setGeometry(QRect(80, 270, 161, 71));
        Ajouter->setFont(font1);
        Quitter_3 = new QPushButton(Utilisateur);
        Quitter_3->setObjectName(QStringLiteral("Quitter_3"));
        Quitter_3->setGeometry(QRect(620, 400, 86, 28));
        Quitter_3->setFont(font1);
        verticalLayoutWidget = new QWidget(Utilisateur);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(400, 130, 171, 101));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        UtilisateurAdd = new QLabel(verticalLayoutWidget);
        UtilisateurAdd->setObjectName(QStringLiteral("UtilisateurAdd"));
        UtilisateurAdd->setEnabled(true);
        UtilisateurAdd->setFont(font1);

        verticalLayout->addWidget(UtilisateurAdd);

        ErreurAjouter = new QLabel(verticalLayoutWidget);
        ErreurAjouter->setObjectName(QStringLiteral("ErreurAjouter"));
        ErreurAjouter->setFont(font1);

        verticalLayout->addWidget(ErreurAjouter);

        ErreurSaisie = new QLabel(verticalLayoutWidget);
        ErreurSaisie->setObjectName(QStringLiteral("ErreurSaisie"));
        ErreurSaisie->setFont(font2);

        verticalLayout->addWidget(ErreurSaisie);

        horizontalLayoutWidget_13 = new QWidget(Utilisateur);
        horizontalLayoutWidget_13->setObjectName(QStringLiteral("horizontalLayoutWidget_13"));
        horizontalLayoutWidget_13->setGeometry(QRect(390, 20, 191, 61));
        horizontalLayout_13 = new QHBoxLayout(horizontalLayoutWidget_13);
        horizontalLayout_13->setSpacing(6);
        horizontalLayout_13->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_13->setObjectName(QStringLiteral("horizontalLayout_13"));
        horizontalLayout_13->setContentsMargins(0, 0, 0, 0);
        label_7 = new QLabel(horizontalLayoutWidget_13);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setFont(font2);

        horizontalLayout_13->addWidget(label_7);

        spinBoxID = new QSpinBox(horizontalLayoutWidget_13);
        spinBoxID->setObjectName(QStringLiteral("spinBoxID"));

        horizontalLayout_13->addWidget(spinBoxID);

        tabWidget->addTab(Utilisateur, QString());
        Statistiques = new QWidget();
        Statistiques->setObjectName(QStringLiteral("Statistiques"));
        widget = new QCustomPlot(Statistiques);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(0, 70, 701, 371));
        horizontalLayoutWidget_16 = new QWidget(Statistiques);
        horizontalLayoutWidget_16->setObjectName(QStringLiteral("horizontalLayoutWidget_16"));
        horizontalLayoutWidget_16->setGeometry(QRect(20, 10, 251, 41));
        horizontalLayout_16 = new QHBoxLayout(horizontalLayoutWidget_16);
        horizontalLayout_16->setSpacing(6);
        horizontalLayout_16->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_16->setObjectName(QStringLiteral("horizontalLayout_16"));
        horizontalLayout_16->setContentsMargins(0, 0, 0, 0);
        ProduitStat = new QLabel(horizontalLayoutWidget_16);
        ProduitStat->setObjectName(QStringLiteral("ProduitStat"));

        horizontalLayout_16->addWidget(ProduitStat);

        comboBoxProdStat = new QComboBox(horizontalLayoutWidget_16);
        comboBoxProdStat->setObjectName(QStringLiteral("comboBoxProdStat"));

        horizontalLayout_16->addWidget(comboBoxProdStat);

        tabWidget->addTab(Statistiques, QString());
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 767, 25));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        Numero_du_controleur->setText(QApplication::translate("MainWindow", "     Controleur :", nullptr));
        Ref_du_produit->setText(QApplication::translate("MainWindow", "          Produit :", nullptr));
        Envoyer->setText(QApplication::translate("MainWindow", "ENVOYER  RAPPORT", nullptr));
        label->setText(QString());
        Quittez->setText(QApplication::translate("MainWindow", "QUITTER", nullptr));
        PoidsDuProduit->setText(QApplication::translate("MainWindow", "    Poids du produit :", nullptr));
        Service->setText(QApplication::translate("MainWindow", "          Service :", nullptr));
        PoidsDeReference->setText(QApplication::translate("MainWindow", "Poids de r\303\251f\303\251rence :", nullptr));
        Nom_du_poste->setText(QApplication::translate("MainWindow", "Nom du poste :", nullptr));
        IMPRIMER->setText(QApplication::translate("MainWindow", "IMPRIMER", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "               Variation :", nullptr));
        Conforme->setText(QApplication::translate("MainWindow", "CONFORME", nullptr));
        NonConforme->setText(QApplication::translate("MainWindow", "NON CONFORME", nullptr));
        NbDeProduit->setText(QApplication::translate("MainWindow", "Nombre de produit :", nullptr));
        RapportAdd->setText(QApplication::translate("MainWindow", "RAPPORT ENVOY\303\211", nullptr));
        ErreurRap->setText(QApplication::translate("MainWindow", "ERREUR DE SAISIE", nullptr));
        pushButton_2->setText(QApplication::translate("MainWindow", "ACTUALISER ", nullptr));
        pushButton_3->setText(QApplication::translate("MainWindow", "PESER LE PRODUIT", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(Formulaire), QApplication::translate("MainWindow", "Formulaire", nullptr));
        Nom_du_produit->setText(QApplication::translate("MainWindow", "                      Nom du produit :", nullptr));
        Reference->setText(QApplication::translate("MainWindow", "             R\303\251f\303\251rence du produit :", nullptr));
        PoidsProduitRef->setText(QApplication::translate("MainWindow", "Poids du produit de r\303\251f\303\251rence : ", nullptr));
        AjouterProduitRef->setText(QApplication::translate("MainWindow", "AJOUTER", nullptr));
        ErreurProduitRef->setText(QApplication::translate("MainWindow", "Erreur !", nullptr));
        ProduitAdd->setText(QApplication::translate("MainWindow", "Produit ajout\303\251 !", nullptr));
        pushButton->setText(QApplication::translate("MainWindow", "QUITTER", nullptr));
        label_8->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(Produit_de_reference), QApplication::translate("MainWindow", "Ajout produit r\303\251f\303\251rence", nullptr));
        NomUser->setText(QApplication::translate("MainWindow", "            Nom :", nullptr));
        PrenomUser->setText(QApplication::translate("MainWindow", "       Pr\303\251nom :", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "Adresse mail :", nullptr));
        label_6->setText(QString());
        Ajouter->setText(QApplication::translate("MainWindow", "AJOUTER", nullptr));
        Quitter_3->setText(QApplication::translate("MainWindow", "QUITTER", nullptr));
        UtilisateurAdd->setText(QApplication::translate("MainWindow", "Utilisateur ajout\303\251 !", nullptr));
        ErreurAjouter->setText(QApplication::translate("MainWindow", "Erreur d'ajout !", nullptr));
        ErreurSaisie->setText(QApplication::translate("MainWindow", "Erreur de saisie !", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "Identifiant :", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(Utilisateur), QApplication::translate("MainWindow", "Ajout utilsateur", nullptr));
        ProduitStat->setText(QApplication::translate("MainWindow", "Produit :", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(Statistiques), QApplication::translate("MainWindow", "Statistiques", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
