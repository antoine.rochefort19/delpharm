#include "tbalance.h"

tbalance::tbalance(string uri)
{
    URI = new char[uri.length()+1];
    strcpy(URI,uri.c_str());

    poids = "0";
}

int tbalance::write(char* data,size_t size,size_t nmemb,string* buffer){
    int result = 0;
    if (buffer != NULL)
    {
        buffer->append(data, size * nmemb);
        result = size * nmemb;
    }
    return result;
}

void tbalance::decoupage_trame(){
    short size_word = (poids.length()-10)-3;

    poids = poids.substr(10,size_word);
}

void tbalance::weight_product(){

    CURL* request = curl_easy_init();
    CURLcode resultat;
    string buffer;


    curl_global_init(CURL_GLOBAL_DEFAULT);
    curl_easy_setopt(request, CURLOPT_URL, URI);
    curl_easy_setopt(request, CURLOPT_CUSTOMREQUEST, "GET");


    curl_easy_setopt(request,CURLOPT_WRITEFUNCTION,write);
    curl_easy_setopt(request, CURLOPT_WRITEDATA, &buffer);

    resultat = curl_easy_perform(request);//http get performed

    //Clear de la requête
    curl_easy_cleanup(request);

    poids = buffer;

}

string tbalance::get_poids(){
    weight_product();
    decoupage_trame();
    return poids;
}

