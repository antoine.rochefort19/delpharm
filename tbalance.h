#ifndef TBALANCE_H
#define TBALANCE_H

#include <iostream>
#include <stdio.h>
#include <curl/curl.h>
#include "string.h"

using namespace std;

class tbalance
{

private:

    static int write(char*,size_t,size_t,string*);
    void decoupage_trame();
    void weight_product();

    char* URI;
    string poids;

public:

    tbalance(string);
    string get_poids();

};

#endif // TBALANCE_H
